from typing import List

from sqlalchemy import create_engine, select
from sqlalchemy.orm import Session

from orm import DBCity, DBState, DBCounty
from rest_entities import RESTCounty, RESTCity, RESTState


class NoResultsException(Exception):
    message: str = ""

    def __init__(self, message: str):
        self.message = message


class Database:
    def __init__(self, engine_url: str) -> None:
        self.engine = create_engine(engine_url, echo=True)

    def get_cities(self, limit: int = 20, offset: int = 0) -> List[RESTCity]:
        with Session(self.engine) as session:
            stmt = select(DBCity).limit(limit).offset(offset)
            res = session.scalars(stmt)
            return [_create_rest_city(r) for r in res]

    def get_counties(self, limit: int = 20, offset: int = 0) -> List[RESTCounty]:
        with Session(self.engine) as session:
            stmt = select(DBCounty).limit(limit).offset(offset)
            res = session.scalars(stmt)
            return [_create_rest_county(r) for r in res]

    def get_states(self, limit: int = 20, offset: int = 0) -> List[RESTState]:
        with Session(self.engine) as session:
            stmt = select(DBState).limit(limit).offset(offset)
            res = session.scalars(stmt)
            return [_create_rest_state(r) for r in res]

    def search_cities(self,
                     state: str | None,
                     county: str | None,
                     limit: int = 100,
                     offset: int = 0) -> List[RESTCity]:
        with Session(self.engine) as session:
            stmt = select(DBCity) \
                .join(DBCounty) \
                .join(DBState) \
                .limit(limit).offset(offset)
            if state: stmt = stmt.filter(DBState.name.like(f"%{state}%"))
            if county: stmt = stmt.filter(DBCounty.name.like(f"%{county}%"))
            ret = session.scalars(stmt)
            return [_create_rest_city(r) for r in ret]


def _create_rest_city(dbcity: DBCity) -> RESTCity:
    return RESTCity(id=dbcity.id, name=dbcity.city, population=dbcity.population)


def _create_rest_county(dbcounty: DBCounty) -> RESTCounty:
    return RESTCounty(fips=dbcounty.fips, name=dbcounty.name)


def _create_rest_state(dbstate: DBState) -> RESTState:
    return RESTState(id=dbstate.id, name=dbstate.id)

