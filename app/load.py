import csv
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from orm import DBCityRaw


def main() -> None:
    engine = create_engine("postgresql://local:pass@localhost/cities", echo=False)
    with Session(engine) as session:
        with open('../simplemaps_uscities_basicv1.76/uscities.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    pass
                else:
                    city: DBCityRaw = DBCityRaw(
                        city=row[0],
                        city_ascii=row[1],
                        state_id=row[2],
                        state_name=row[3],
                        county_fips=row[4],
                        county_name=row[5],
                        lat=row[6],
                        lng=row[7],
                        population=row[8],
                        density=row[9],
                        source=row[10],
                        incorporated=row[11] == 'TRUE',
                        timezone=row[12],
                        ranking=row[13],
                        zips=row[14],
                        id=row[15]
                    )
                    session.merge(city)
                line_count += 1
            session.commit()
            print(f'Processed {line_count} lines.')


if __name__ == "__main__":
    main()
