from fastapi import FastAPI
from typing import List
from database import Database
from rest_entities import RESTMessage, RESTCity, RESTState, RESTCounty

app = FastAPI()
database: Database = Database("postgresql://local:pass@localhost/cities")


@app.get("/")
def root() -> RESTMessage:
    return RESTMessage(message="Hello World!")


@app.get("/cities")
def endpoint_cities(limit: int = 100, offset: int = 0) -> List[RESTCity]:
    cities: List[RESTCity] = database.get_cities(limit=check_and_fix_query_limit(limit), offset=offset)
    return cities


@app.get("/counties")
def endpoint_counties(limit: int = 100, offset: int = 0) -> List[RESTCounty]:
    counties: List[RESTCounty] = database.get_counties(limit=check_and_fix_query_limit(limit), offset=offset)
    return counties


@app.get("/states")
def endpoint_states(limit: int = 100, offset: int = 0) -> List[RESTState]:
    states: List[RESTState] = database.get_states(limit=check_and_fix_query_limit(limit), offset=offset)
    return states


@app.get("/search/cities")
def endpoint_search_cities(state: str | None = None,
           county: str | None = None,
           limit: int = 100,
           offset: int = 0) -> List[RESTCity]:
    cities: List[RESTCity] = database.search_cities(state=state, county=county, limit=check_and_fix_query_limit(limit), offset=offset)
    return cities


def check_and_fix_query_limit(limit: int, max_limit: int = 200) -> int:
    if limit < 1: return 1
    if limit > max_limit: return max_limit
    return limit
