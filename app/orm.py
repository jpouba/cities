from sqlalchemy import ForeignKey
from sqlalchemy import String
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column


class Base(DeclarativeBase):
    pass


class DBCityRaw(Base):
    __tablename__ = "cities_raw"
    id: Mapped[int] = mapped_column(name="id", primary_key=True)
    city: Mapped[str] = mapped_column(name="city", type_=String(250), nullable=False)
    city_ascii: Mapped[str] = mapped_column(name="city_ascii", type_=String(250), nullable=False)
    state_id: Mapped[str] = mapped_column(name="state_id", type_=String(250), nullable=False)
    state_name: Mapped[str] = mapped_column(name="state_name", type_=String(250), nullable=False)
    county_fips: Mapped[int] = mapped_column(name="county_fips", nullable=False)
    county_name: Mapped[str] = mapped_column(name="county_name", type_=String(250), nullable=False)
    lat: Mapped[float] = mapped_column(name="lat", nullable=False)
    lng: Mapped[float] = mapped_column(name="lng", nullable=False)
    population: Mapped[int] = mapped_column(name="population", nullable=False)
    density: Mapped[float] = mapped_column(name="density", nullable=False)
    source: Mapped[str] = mapped_column(name="source", type_=String(250), nullable=False)
    incorporated: Mapped[bool] = mapped_column(name="incorporated", nullable=False)
    timezone: Mapped[str] = mapped_column(name="timezone", type_=String(250), nullable=False)
    ranking: Mapped[int] = mapped_column(name="ranking", nullable=False)
    zips: Mapped[str] = mapped_column(name="zips", nullable=False)

    def __repr__(self) -> str:
        return f"City(id={self.id!r}, name={self.city!r}, state={self.state_name!r})"


class DBCity(Base):
    __tablename__ = "cities"
    id: Mapped[int] = mapped_column(name="id", primary_key=True)
    city: Mapped[str] = mapped_column(name="city", type_=String(250), nullable=False)
    city_ascii: Mapped[str] = mapped_column(name="city_ascii", type_=String(250), nullable=False)
    county: Mapped[int] = mapped_column(ForeignKey("counties.fips"))
    lat: Mapped[float] = mapped_column(name="lat", nullable=False)
    lng: Mapped[float] = mapped_column(name="lng", nullable=False)
    population: Mapped[int] = mapped_column(name="population", nullable=False)
    density: Mapped[float] = mapped_column(name="density", nullable=False)
    source: Mapped[str] = mapped_column(name="source", type_=String(250), nullable=False)
    incorporated: Mapped[bool] = mapped_column(name="incorporated", nullable=False)
    timezone: Mapped[str] = mapped_column(name="timezone", type_=String(250), nullable=False)
    ranking: Mapped[int] = mapped_column(name="ranking", nullable=False)
    zips: Mapped[str] = mapped_column(name="zips", nullable=False)

    def __repr__(self) -> str:
        return f"City(id={self.id!r}, id={self.id!r}, name={self.city!r})"


class DBCounty(Base):
    __tablename__ = "counties"
    fips: Mapped[int] = mapped_column(name="fips", primary_key=True)
    name: Mapped[str] = mapped_column(name="name", type_=String(250), nullable=False)
    state: Mapped[int] = mapped_column(ForeignKey("states.id"))

    def __repr__(self) -> str:
        return f"County(fips={self.fips!r}, name={self.name!r})"


class DBState(Base):
    __tablename__ = "states"
    id: Mapped[str] = mapped_column(name="id", type_=String(250), primary_key=True)
    name: Mapped[str] = mapped_column(name="name", type_=String(250), nullable=False)

    def __repr__(self) -> str:
        return f"County(id={self.id!r}, name={self.name!r})"

