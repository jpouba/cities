import pydantic


class RESTMessage(pydantic.BaseModel):
    type: str = "Message"
    message: str


class RESTCity(pydantic.BaseModel):
    id: int
    name: str
    population: int


class RESTCounty(pydantic.BaseModel):
    fips: int
    name: str


class RESTState(pydantic.BaseModel):
    id: str
    name: str

