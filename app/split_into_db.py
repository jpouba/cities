from sqlalchemy import create_engine, select
from sqlalchemy.orm import Session

from orm import DBCityRaw, DBCity, DBState, DBCounty


def main() -> None:
    engine = create_engine("postgresql://local:pass@localhost/cities", echo=False)
    count = 0
    with Session(engine) as session:
        stmt = select(DBCityRaw)
        for city_raw in session.scalars(stmt):
            city: DBCity = DBCity(
                id=city_raw.id,
                city=city_raw.city,
                city_ascii=city_raw.city_ascii,
                county=city_raw.county_fips,
                lat=city_raw.lat,
                lng=city_raw.lng,
                population=city_raw.population,
                density=city_raw.density,
                source=city_raw.source,
                incorporated=city_raw.incorporated,
                timezone=city_raw.timezone,
                ranking=city_raw.ranking,
                zips=city_raw.zips
            )
            county: DBCounty = DBCounty(
                fips=city_raw.county_fips,
                name=city_raw.county_name,
                state=city_raw.state_id,
            )
            state: DBState = DBState(
                id=city_raw.state_id,
                name=city_raw.state_name,
            )
            session.merge(state)
            session.merge(county)
            session.merge(city)
            count = count + 1
        session.commit()
        print(f"Processed {count} cities")


if __name__ == "__main__":
    main()
