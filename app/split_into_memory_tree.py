from dataclasses import dataclass
from typing import Any, Dict

from sqlalchemy import create_engine, select
from sqlalchemy.orm import Session

from orm import DBCityRaw


@dataclass
class City:
    id: int
    name: str
    population: int
    county: Any


@dataclass
class County:
    fips: int
    name: str
    state: Any
    cities: Dict[int, City]


@dataclass
class State:
    id: str
    name: str
    counties: Dict[int, County]


def main() -> None:
    engine = create_engine("postgresql://local:pass@localhost/cities", echo=False)
    count = 0
    states: Dict[str, State] = {}
    counties: Dict[int, County] = {}
    cities: Dict[int, City] = {}
    with Session(engine) as session:
        stmt = select(DBCityRaw)
        for city_raw in session.scalars(stmt):
            state: State = get_or_create_state(id=city_raw.state_id, name=city_raw.state_name, states=states)
            county: County = get_or_create_county(fips=city_raw.county_fips, name=city_raw.county_name, state=state, counties=counties)
            city: City = get_or_create_city(id=city_raw.id, name=city_raw.city, population=city_raw.population, county=county, cities=cities)

            count = count + 1
        print(f"Processed {count} cities")

    # Print all recorded cities
    for s in states.values():
        print(f"{s.id}, {s.name}")
        for c in s.counties.values():
            print(f"  {c.fips}, {c.name}")
            print(f"    {[ct.name for ct in c.cities.values()]}")

    # Print county info
    print("***")
    print(states["DE"].counties[10001])

    # Print all cities in a given county and state
    print("***")
    print([ct.name for ct in states["SD"].counties[46099].cities.values()])


def get_or_create_state(id: str, name: str, states: Dict[str, State]) -> State:
    state: State | None = states.get(id)
    if state: return state
    state = State(id=id, name=name, counties={})
    states[id] = state
    return state


def get_or_create_county(fips: int, name: str, state: State, counties: Dict[int, County]) -> County:
    county: County | None = counties.get(fips)
    if county: return county
    county = County(fips=fips, name=name, state=state, cities={})
    state.counties[county.fips] = county
    counties[fips] = county
    return county


def get_or_create_city(id: int, name: str, population: int, county: County, cities: Dict[int, City]) -> City:
    city: City | None = cities.get(id)
    if city: return city
    city = City(id=id, name=name, county=county, population=population)
    county.cities[city.id] = city
    cities[id] = city
    return city


if __name__ == "__main__":
    main()
